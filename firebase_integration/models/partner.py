# -*- coding: utf-8 -*-
import firebase_admin, os
from firebase_admin import credentials, firestore

from odoo import models, fields, api


class Partner(models.Model):
    _inherit = 'res.partner'

    @api.model
    def create(self, values):
        res = super(Partner, self).create(values)
        try:
            cred = credentials.Certificate(
                os.path.join(os.path.dirname(__file__)) + "/odoo-elmenshawy-firebase-adminsdk-69a21-c3da08152e.json")
            firebase_admin.initialize_app(cred)
        except:
            pass
        db = firestore.client()
        collection_ref = db.collection(u'res_partners')
        doc_ref = collection_ref.document(res.name)
        doc = doc_ref.get()
        if doc.exists:
            print(f'Document data: {doc.to_dict()}')
        else:
            doc_ref.set({
                'name': res.name,
                'odoo_id': res.id
            })
        return res
