# -*- coding: utf-8 -*-

from odoo import http, fields, _
from odoo.http import request, Response
import logging, json

_logger = logging.getLogger(__name__)


class Partner(http.Controller):

    @http.route('/partner/create_new_partner', auth='public', csrf=False, type='http', methods=['POST'])
    def create_new_partner(self, **kwargs):
        name = kwargs.get('name', False)
        result = {}
        if name:
            partner_id = request.env['res.partner'].sudo().search([('name', '=', name)])
            if not partner_id:
                partner_id = request.env['res.partner'].sudo().create({'name': name})
                result['message'] = 'Partner created successfully'
            else:
                result['message'] = 'Partner already exist'
            result['partner_id'] = partner_id.id
            result['response_code'] = 200
        else:
            result['response_code'] = 202
            result['message'] = 'Can not create partner'
        return request.make_response(json.dumps(result))
