# -*- coding: utf-8 -*-
{
    'name': "Firebase Integration",
    'summary': """
        integrate partner profile with firebase
        """,
    'author': "Mahmoud Elmenshawy",
    'website': "https://www.linkedin.com/in/mahmoudelmenshawy/",
    'category': 'Integration',
    'version': '0.1',
    'depends': ['contacts'],
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
    ],
    'demo': [
    ],
}
